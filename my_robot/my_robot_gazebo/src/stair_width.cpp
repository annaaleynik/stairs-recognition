#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv4/imgproc/imgproc.hpp>
#include <opencv4/highgui.hpp>
#include <stdio.h>

using namespace cv;
using namespace std;

static const std::string OPENCV_WINDOW = "Image window";
static const float focalLength = 476.703084;
static const float sensorHeigth = 4000;
// static const float imageHeigth = 708;
static const float imageHeigth = 800;
// static const float distance = 4900;
static const float distanceToObj = 4000;

class ImageConverter
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;

public:
  ImageConverter()
    : it_(nh_)
  {
    // Subscrive to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/my_robot_2/camera1/image_raw", 1,
      &ImageConverter::imageCb, this);
    image_pub_ = it_.advertise("/image_converter/output_video", 1);

    namedWindow(OPENCV_WINDOW);
  }

  ~ImageConverter()
  {
    destroyWindow(OPENCV_WINDOW);
  }

  static bool comp (Vec4i a, Vec4i b) {
    return a[1] > b[1];
  }

  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    Mat img = cv_ptr->image;

    imshow("Source",img);

    GaussianBlur(img,img,Size(5,5),0,0);

    Mat dst, out_img,control;
    Canny(img, dst, 80, 240, 3);
    cvtColor(dst, out_img, cv::COLOR_GRAY2BGR);

    vector<int> y_keeper_for_lines;
    vector<Vec4i> corectLines;
    vector<Vec4i> lines;
    HoughLinesP(dst, lines, 1, CV_PI/180, 30, 40, 5 );

    Vec4i l = lines[0];
    y_keeper_for_lines.push_back(l[1]);
    corectLines.push_back(l);

    int okey = 1;
    int stair_counter = 1;


    for( size_t i = 1; i < lines.size(); i++ )
    {
        Vec4i l = lines[i];
        for(int m:y_keeper_for_lines)
        {
            if(abs(m-l[1])<15)
                okey = 0;
        }
        if(okey)
        {
            corectLines.push_back(l);
        }
        okey = 1;
    }

    sort(corectLines.begin(), corectLines.end(), comp);


    Vec4i line_first = corectLines[0];
    Vec4i line_last = corectLines[corectLines.size() - 1];

    line( out_img, Point(0, line_first[1]), Point(img.cols, line_first[1]), Scalar(0,0,255), 3, cv::LINE_AA);
    line( out_img, Point(0, line_last[1]), Point(img.cols, line_last[1]), Scalar(0,0,255), 3, cv::LINE_AA);


    float h;
   
    float objectHeight = abs(line_last[1] - line_first[1]);

    
    h = distanceToObj * objectHeight * sensorHeigth / (focalLength * imageHeigth);

    putText(out_img,"Stair width:" + to_string(h),Point(40,60),FONT_HERSHEY_SIMPLEX,1.5,Scalar(0,255,0),2);

    imshow(OPENCV_WINDOW, out_img);
    
    waitKey(3);

    image_pub_.publish(cv_ptr->toImageMsg());
  }
};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "image_converter");
  ImageConverter ic;
  ros::spin();
  return 0;
}